package it.stefanobizzi;

public class MainChar {
    public static void main(String[] args) {
        char c = 'C';
        char i = '\u0049';
        char a = '\101';
        char o = 79;

        System.out.print(c);
        System.out.print('\t');
        System.out.print(i);
        System.out.print(a);
        System.out.print('\b');
        System.out.println(o);
        System.out.println(c+i+a+o); // Somma dei codici di tutti i char

        // reh 0550
        // psi 0470
        char reh = '\u0550';
        char psi = '\u0470';
        System.out.println(reh);
        System.out.println(psi);

        byte letteraO = 79;
        // char letteraOChar = letteraO; <- richiede typecasting esplicito
    }
}
