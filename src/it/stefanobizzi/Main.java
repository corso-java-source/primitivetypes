package it.stefanobizzi;

public class Main {
    boolean myState;

    public static void main(String[] args) {
        byte myVar = 9;
        byte mySecondVar;

        System.out.println(myVar);
        mySecondVar = 14;
        System.out.println(mySecondVar);

        Main main = new Main();
        System.out.println(main.myState);
    }
}
