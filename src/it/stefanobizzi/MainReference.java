package it.stefanobizzi;

public class MainReference {
    public static void main(String[] args) {
        SampleClass sampleClass = new SampleClass();
        System.out.println(sampleClass.value);
        SampleClass sampleClass1 = sampleClass;
        sampleClass1.value = 20;
        System.out.println(sampleClass.value);
    }
}

class SampleClass {
    int value = 10;
}
