package it.stefanobizzi;

public class MainNumbers {
    public static void main(String[] args) {
        short byteTooBig = 900;
        int intVar = byteTooBig;
        // short error = intVar; -> richiede typecasting esplicito, affrontato nella lezione 6

        byte var = 12;


        // Esprimiamo il numero 15 in quattro basi: 10, 8, 2 e 16:
        byte quindici = 15;
        byte quindiciOctal = 017;
        byte quindiciBinary = 0b1111;
        byte quindiciHex = 0xF;

        System.out.println(quindici);
        System.out.println(quindiciOctal);
        System.out.println(quindiciHex);
        System.out.println(quindiciBinary);

        float myFloat = 768.98F;
        float myFloatExp = 7.6898E2F;
        double doubleVar = myFloat;

        System.out.println(myFloat);
        System.out.println(myFloatExp);
        System.out.println(doubleVar); // <- aggiunge precisione

    }
}
